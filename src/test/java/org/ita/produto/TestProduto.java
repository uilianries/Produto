/*!
 * @file TestProduto.java
 * @brief Testa Produto
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.produto;

import org.junit.Test;

import java.security.InvalidParameterException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestProduto {
    @Test(expected=InvalidParameterException.class)
    public void testProdutoInvalido() {
        new Produto("", 1, 2);
        new Produto("Foobar", 0, 2);
        new Produto("Foobar", 1, -1);
    }

    @Test
    public void testEquals() {
        Produto lhs = new Produto("Foobar", 42, 32.5);
        Produto rhs = new Produto("Foobar", 42, 32.5);
        assertEquals(lhs, rhs);

        Produto no_lhs = new Produto("Foobar", 31, 32.5);
        assertTrue(!lhs.equals(no_lhs));
    }

    @Test
    public void testHashCode() {
        Produto produto = new Produto("Foobar", 42, 52);
        assertEquals(42, produto.hashCode());
    }
}
