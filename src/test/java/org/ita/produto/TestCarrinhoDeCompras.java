/*!
 * @file TestCarrinhoDeCompras.java
 * @brief Testa o Carrinho de compras
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.produto;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCarrinhoDeCompras {

    @Test
    public void testValorTotal() {
        CarrinhoDeCompras carrinho = new CarrinhoDeCompras();
        carrinho.adicionaProduto(new Produto("Chuveiro", 23, 84), 2);
        carrinho.adicionaProduto(new Produto("Lampada", 58, 12), 20);

        assertEquals(408, carrinho.calculaValorTotal(), 0.001);

        carrinho.removeProduto(new Produto("Chuveiro", 23, 84), 1);
        assertEquals(324, carrinho.calculaValorTotal(), 0.001);

        carrinho.adicionaProduto(new ProdutoComTamanho("Corda", 102, 12, 1), 3);
        assertEquals(360, carrinho.calculaValorTotal(), 0.001);
    }
}
