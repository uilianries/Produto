/*!
 * @file TestProdutoComTamanho.java
 * @brief Testa produto com campo de tamanho
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.produto;

import org.junit.Test;
import java.security.InvalidParameterException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/*!
 * @brief Valida produto com tamanho
 */
public class TestProdutoComTamanho {

    @Test(expected = InvalidParameterException.class)
    public void testTamanhoInvalido() {
        ProdutoComTamanho produto = new ProdutoComTamanho("Foobar", 42, 5.00, -1);
    }

    @Test
    public void testEquals() {
        ProdutoComTamanho lhs = new ProdutoComTamanho("Foobar", 42, 32.5, 4);
        ProdutoComTamanho rhs = new ProdutoComTamanho("Foobar", 42, 32.5, 4);
        assertEquals(lhs, rhs);

        ProdutoComTamanho no_lhs = new ProdutoComTamanho("Foobar", 42, 32.5, 5);
        assertTrue(!lhs.equals(no_lhs));
    }

    @Test
    public void testHashCode() {
        ProdutoComTamanho lhs = new ProdutoComTamanho("Foobar", 42, 32.5, 4);
        assertEquals(2267, lhs.hashCode());
        lhs = new ProdutoComTamanho("Foobar", 42, 32.5, 5);
        assertEquals(2268, lhs.hashCode());
    }

}
