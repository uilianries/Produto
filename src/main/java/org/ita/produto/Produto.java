/*!
 * @file Produto.java
 * @brief Produto geral de cadastro
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.produto;

import java.security.InvalidParameterException;

/*!
 * @brief Produto único
 */
public class Produto {
    private String nome;
    private int codigo;
    private double preco;

    /*!
     * @brief Construtor do produto
     * @param nome      Nome do produto
     * @param codigo    Identificador unico do produto
     * @param preco     Valor do produto
     */
    public Produto(String nome, int codigo, double preco) {
        if ("".equals(nome)) {
            throw new InvalidParameterException("Nome vazio");
        }

        if (codigo < 1) {
            throw new InvalidParameterException("Codigo invalido");
        }

        if (preco < 0) {
            throw new InvalidParameterException("Preco negativo");
        }

        this.nome = nome;
        this.codigo = codigo;
        this.preco = preco;
    }

    /*!
     * @brief Getter para preco do produto
     */
    public double getPreco() {
        return this.preco;
    }

    /*!
     * @brief Getter para nome do produto
     */
    public String getNome() {
        return this.nome;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            throw new NullPointerException("Produto nulo.");
        }

        if (!(obj instanceof Produto)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        Produto produto = (Produto) obj;
        return produto.codigo == this.codigo;
    }

    @Override
    public int hashCode() {
        return this.codigo;
    }

}
