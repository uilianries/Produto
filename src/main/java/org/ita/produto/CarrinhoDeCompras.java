/*!
 * @file Produto.java
 * @brief Produto geral de cadastro
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.produto;


import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

public class CarrinhoDeCompras {
    private HashMap<Produto, Integer> tabelaDeCompra = new HashMap<Produto, Integer>();

    /*!
     * @brief Adiciona um novo produto no carrinho
     * @param produto       Produto para ser adicionado
     * @param quantidade    Quantidade do produto
     * @exception InvalidParameterException
     * @exception NullPointerException
     */
    public void adicionaProduto(Produto produto, int quantidade) {
        if (quantidade < 0) {
            throw new InvalidParameterException("Quantidade negativa");
        }
        if (produto == null) {
            throw new NullPointerException("Produto nulo");
        }

        if (this.tabelaDeCompra.containsKey(produto)) {
            quantidade += this.tabelaDeCompra.get(produto);
        }
        this.tabelaDeCompra.put(produto, quantidade);
    }

    /*!
    * @brief Remove um produto no carrinho
    * @param produto       Produto para ser adicionado
    * @param quantidade    Quantidade do produto
    * @exception InvalidParameterException
    * @exception NullPointerException
    */
    public void removeProduto(Produto produto, int quantidade) {
        if (quantidade < 0) {
            throw new InvalidParameterException("Quantidade negativa");
        }
        if (produto == null) {
            throw new NullPointerException("Produto nulo");
        }

        if (this.tabelaDeCompra.containsKey(produto)) {
            quantidade = this.tabelaDeCompra.get(produto) - quantidade;
            if (quantidade < 0) {
                quantidade = 0;
            }
            this.tabelaDeCompra.put(produto, quantidade);
        }
    }

    /*!
     * @brief Calcula valor total do carrinho
     * @return Valor contido no carrinho
     */
    public double calculaValorTotal() {
        double total = 0;

        for (Map.Entry<Produto, Integer> produto : this.tabelaDeCompra.entrySet()) {
            total += produto.getKey().getPreco() * produto.getValue();
        }

        return total;
    }

    @Override
    public String toString() {
        String result = "";

        for (Map.Entry<Produto, Integer> produto : this.tabelaDeCompra.entrySet()) {
            result += produto.getKey().getNome() + ":\t\t" + produto.getValue() + '\n';
        }

        return result;
    }
}
