/*!
 * @file ProdutoComTamanho.java
 * @brief Descrição de produtc com tamanho
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.produto;

import java.security.InvalidParameterException;
import java.util.Objects;

/*!
 * @brief Produto com Tamanho
 */
public class ProdutoComTamanho extends Produto {
    private int tamanho;

    public ProdutoComTamanho(String nome, int codigo, double preco, int tamanho) {
        super(nome, codigo, preco);
        if (tamanho < 0) {
            throw new InvalidParameterException("Tamanho negativo");
        }
        this.tamanho = tamanho;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if (super.equals(obj)) {
            ProdutoComTamanho produto = (ProdutoComTamanho) obj;
            if (this.tamanho == produto.tamanho) {
                result = true;
            }
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.tamanho);
    }
}
