/*!
 * @file Principal.java
 * @brief Executa uma compra
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.produto;


public class Principal {
    static public void main(String [] args) {

        CarrinhoDeCompras carrinho = new CarrinhoDeCompras();
        carrinho.adicionaProduto(new Produto("Mochila", 51, 120.0), 2);
        carrinho.adicionaProduto(new Produto("Camisa", 23, 8.0), 50);
        carrinho.adicionaProduto(new ProdutoComTamanho("Sapato", 42, 58.0, 40), 3);

        System.out.println("Total da compra (R$): " + carrinho.calculaValorTotal());
        System.out.println("Relaçao de Produtos:\n" + carrinho);
    }
}
